## 闭包
高阶函数除了可以接受函数作为参数外，还可以把函数作为结果值返回。
1.是一个被（外部）函数返回的函数
2.这个被返回的函数，使用了外部函数的变量或者是参数，使得用到的变量或者参数不能释放、
这样的一个结构叫闭包

```
function count() {
    var arr = [];
    for (let i=1; i<=3; i++) {
        arr.push(function () {
            return i * i;
        });
    }
    return arr;
}

var results = count();
var f1 = results[0];
var f2 = results[1];
var f3 = results[2];
console.log(f1()); 效果为1
console.log(f2()); 效果为4
console.log(f3()); 效果为9
```


```

function createCounter(init){
    let init =init || 0;//默认值，当init没有传入的时候，init的值为0
    return{
        increment:function(){
            return init+1;
        }
    }
}
let c1=createCounter(); 没有值 则为0 因为return init+1  所以0+1=1
let c2=createCounter(10); 值为10 因为return init+1 所以10+1=11
console.log(c1.createCounter()); 结果为1
console.log(c2.createCounter()); 结果为11

```


```
let x=Math.pow(2,8);
console.log(x); 效果为2的8次方等于256
```


```
function create(x,y){
    function tmp(x){
        return Math.pow(x,y);
    }
    return tmp;
}
let pow2=createPow(2);
console.log(pow2(2));效果为2的2次方等于4
console.log(pow2(3));效果为3的2次方等于8
console.log(pow2(4));效果为4的2次方等于16

let pow3=createPow(3);
console.log(pow3(1));效果为1的3次方等于1
console.log(pow3(2));效果为2的3次方等于8
console.log(pow3(3));效果为3的3次方等于27

```

```
//for循环中的定时器 观察以下代码，提出修改方案，要求按预期输出1、2、3：
for(let i = 1; i < 4; i++) {
    setTimeout(function(){
      console.log(i);
    });
}
```