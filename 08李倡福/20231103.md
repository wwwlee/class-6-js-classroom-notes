## 数组
```
push()向Array的末尾添加若干元素
var arr=[1,9,18];
arr.push('你好',100,200)
console.log(arr);  结果为[1,9,18,'你好',100,200]
console.log(arr.length);  结果为6

pop()则把Array的最后一个元素删除掉：
var num=arr.pop();
console.log(num);  结果为200
console.log(arr);  结果为[1,9,18,'你好',100]
```

```
sort()可以对当前Array进行排序，它会直接修改当前Array的元素位置，直接调用时，按照默认顺序排序：
var arr=['n','d','h','a','b']
arr.sort()
console.log(arr);  结果为['a','b','d','h','n']
```

```
splice()方法是修改Array的“万能方法”，它可以从指定的索引开始删除若干元素，然后再从该位置添加若干元素：
var arr=[1,9,18,5,21,52]
console.log(arr);
let res=arr.splice(0,5)
console.log(res) 结果为[,9,18,5,21]  删除的元素
console.log(arr)  结果为[52]  剩余元素

var arr=[1,9,18,5,21,52]
let result=arr.splice(1,0,100,200,300)  (第1位开始，要删除的数量，要添加的数，要添加的数，要添加的数，)
console.log(arr) 结果为[1,100,200,300,9,18,5,21,52]
```

```
var arr=['赵','冬','莹']
let result=arr.splice(2,0,'今')  (从第二位开始,要删除的数量，要添加的数)
console.log(arr)  结果为['赵','冬','今','莹']
```

```
concat()方法把当前的Array和另一个Array连接起来，并返回一个新的Array：
var arr=['可','以','吗']
let result2=arr.concat(['不','可','以'])
console.log(result2);  结果为['可','以','吗','不','可','以']
```

```
join()方法是一个非常实用的方法，它把当前Array的每个元素都用指定的字符串连接起来，然后返回连接后的字符串：
var arr=['可','以','吗']
var result3=arr.join('-') 添加‘-’
console.log(result3); 结果为 可-以-吗
var other=result3.split('-') 删除‘-’
console.log(other); 结果为 '可','以','吗'
```

## 对象
```
JavaScript用一个{...}表示一个对象，键值对以xxx: xxx形式申明，用,隔开。注意，最后一个键值对不需要在末尾加,，如果加了，有的浏览器（如低版本的IE）将报错。
var obj={
    name:'李白',
    age:23,
    height:178
}
console.log(obj); 结果为 {name: '李白', age: 23, height: 178}
console.log(obj.name);  结果为'李白'
console.log(obj['name']);  结果为'李白'
obj.name='小白'
obj.width=128
console.log(obj);  结果为  {name: '小白', age: 23, height: 178,width:128}
var res1='name' in obj;  ‘name’ 有没有在obj里面
console.log(res1); 结果为true
var res2='grade' in obj;  ‘grade’ 有没有在obj里面
console.log(res2); 结果为false

```


## 条件判断
```
JavaScript使用if () { ... } else { ... }来进行条件判断。
var age=18;
var obj1={
    name:'赵冬莹',
    age:15
}
if(obj1.age>=age){
    console.log('这个同学长大了');
}else{
    console.log('小屁孩');
}
结果为 小屁孩
```

## for循环
```
//for循环
var x=0;
var i;
for(i=1;i<5;i++){
    x=x+i;
}
console.log(x); 结果为10
```



## 作业

```
 练习：如何通过索引取到500这个值
var arr = [[1, 2, 3], [400, 500, 600], '-'];
var x = (arr[1][1]);
console.log(x); // x应该为500
```


```
练习：在新生欢迎会上，你已经拿到了新同学的名单，请排序后显示：欢迎XXX，XXX，XXX和XXX同学！：
var arr = ['小明', '小红', '大军', '阿黄'];
console.log('欢迎'+arr.slice(0,3)+'和'+arr[3]+'同学');
```



```

小明身高1.75，体重80.5kg。请根据BMI公式（体重除以身高的平方）帮小明计算他的BMI指数，并根据BMI指数：
低于18.5：过轻
18.5-25：正常
25-28：过重
28-32：肥胖
高于32：严重肥胖
用if...else...判断并显示结果：

var weight = parseFloat(prompt('请输入体重(kg):'));
var height = parseFloat(prompt('请输入身高(m):'));
var bmi=(weight/(height*height))
console.log(bmi);
if(bmi<18.5){
    console.log('过轻');
}else if(18.5<=bmi && bmi<25){
    console.log('正常');
}else if(25<=bmi && bmi<28){
    console.log('过重');
}else if(28<=bmi && bmi<32){
    console.log('肥胖');
}else {
    console.log('严重肥胖');
}
```



```
请利用循环遍历数组中的每个名字，并显示Hello, xxx!：
var arr = ['Bart', 'Lisa', 'Adam'];
var i, x;
for (i=0; i<arr.length; i++) {
    x = arr[i];
    console.log('Hello,'+x+'!');
}

```



```

1.数组求和
// 计算并返回给定数组 arr 中所有元素的总和
// 输入描述：
// [ 1, 2, 3, 4 ]
// 输出描述：
// 10

var arr=[1,2,3,4]
console.log(arr[0]+arr[1]+arr[2]+arr[3]);
```



```
2.移除数组中的元素
描述
移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组
输入描述：
[1, 2, 3, 4, 2], 2
输出描述：
[1, 3, 4]

var arr=[[1,2,3,4,2],2]
console.log(arr[0][0],arr[0][2],arr[0][3]);
```



```

3.移除数组中的元素 移除数组 arr 中的所有值与 item 相等的元素，直接在给定的 arr 数组上进行操作，并将结果数组返回
输入：
[1, 2, 2, 3, 4, 2, 2], 2
输出： 
[1, 3, 4]

var arr=[[1,2,2,3,4,2,2],2]
console.log(arr[0][0],arr[0][3],arr[0][4]);
```



```
// 4.数组添加元素 描述
// 在数组 arr 末尾添加元素 item。结果返回新的数组。
// 注意：不要直接修改数组 arr!!!
// 输入描述：
// [1, 2, 3, 4],  10
// 输出描述：
// [1, 2, 3, 4, 10]

var arr=[[1, 2, 3, 4],  10]
var result4=arr.join(',')
console.log(result4);
```


```

// 5.删除数组 arr 最后一个元素。不要直接修改数组 arr，结果返回新的数组

var arr=[11,22,33,44,55]
let res3=arr.splice(4,1)
console.log(arr);
```


```
// 6. 添加元素描述
// 在数组 arr 开头添加元素 item。不要直接修改数组 arr，结果返回新的数组
// 输入：
// [1, 2, 3, 4], 10
// 输出：
// [10, 1, 2, 3, 4]

var arr=[[1, 2, 3, 4], 10]
console.log(arr[1],arr[0][0],arr[0][1],arr[0][2],arr[0][3]);
```


```
// 7. 删除元素 描述
// 删除数组 arr 第一个元素。不要直接修改数组 arr，结果返回新的数组
// 输入描述：
// [1, 2, 3, 4]
// 输出描述：
// [2, 3, 4]
var arr=[1, 2, 3, 4]
let res4=arr.splice(0,1)
console.log(arr);
```


```
 
// 8.数组合并 描述
// 合并数组 arr1 和数组 arr2。不要直接修改数组 arr，结果返回新的数组
// 输入描述：
// [1, 2, 3, 4], ['a', 'b', 'c', 1]
// 输出描述：
// [1, 2, 3, 4, 'a', 'b', 'c', 1]

var arr=[1, 2, 3, 4]
let res5=arr.concat(['a', 'b', 'c', 1])
console.log(res5);

```

```
// 9.在指定位置添加元素
// 描述
// 在数组 arr 的 index 处添加元素 item。不要直接修改数组 arr，结果返回新的数组
// 示例1
// 输入：
// [1, 2, 3, 4], 'z', 2
// 输出：
// [1, 2, 'z', 3, 4]

var arr=[1, 2, 3, 4]
let res6=arr.splice(2,0,'z')
console.log(arr);
```
