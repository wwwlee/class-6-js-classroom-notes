## 搭建网站过程
    'use strict';

    /*
        要建设一个能使用域名访问的网站，需要走通如下条件：
        1.得有一个公网的IP地址，可以在网络上访问到
        2.得有一个域名，域名经过了备案（工信部备案、公安备案）
        3.域名和IP地址绑定，这个过程也叫域名解析
        4.服务器上有web服务器（nginx、apache、tomcat、IIS、Canndy）

            apt install nginx -y

            systemctl status nginx //命令用于查看nginx服务的运行状态
            systemctl start nginx  // 启动nginx服务
            systemctl stop nginx   // 停止nignx服务
            systemctl enable nginx // 将nginx服务设置为开机自动启动
            systemctl disable nginx //将nginx服务设置为禁止开机自启动

        5.服务器上有至少一个web页面（一般是首页index）
            // 网页文件所有目录:/var/www/demo6.9ihub.com/index.html

            scp .\index.html root@demo6.9ihub.com:/var/www/demo6.9ihub.com // 这个命令用于上传本地当前路径下的index.html文件
            到主机demo6.9ihub.com下的指定目录，这个指定的目录为：/var/www/demo6.9ihub.com
        6.为网站创建配置文件（后面以nginx配置文件为主作讲述）
            // 配置文件放在/etc/nginx/conf.d/demo6.9ihub.com.conf

            server {
                listen 80;
                server_name demo6.9ihub.com;

                location / {
                    root /var/www/demo6.9ihub.com;
                    index index.html;
                }

            }

            nginx -t // 用于测试nginx配置文件有没有语法错误
            nginx -s reload // 这个命令用于在不重启nginx的情况下，重新加载配置文件（以让新的配置文件生效，一般用于确认配置文件没有问题后执行）
        7.云服务器安全组设置，允许访问80端口（如果通过80端口访问的话，也可以是其它端口号）

    */