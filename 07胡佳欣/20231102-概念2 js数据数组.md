
## 字符串
JvaScript的字符串就是用''或""括起来的字符表示

### 用转义字符\来标识
```
'I\'m \"OK\"!';
表示的字符串内容是：I'm "OK"!
```
### 多行字符串
多行字符串的表示方法，用反引号 * ... * 表示：
```
多行字符串的表示方法，用反引号 * ... * 表示：
```
### 一些字符串的常用方法
```
1.toUpperCase():把一个字符串全部变为的大写
2.toLowerCase():把一个字符串全部变为小写
3.indexOf：会搜索指定字符串出现的位置
4.substring:返回指定索引区间的子串
```
|转义字符|描述|
|-|-|
|\b|退格|
|\n|换行符|
|\t|水平制表符，Tab空格|
|\f|换页|
|\'|单引号|
|\"|双引号|
|\v|垂直制表符|
|\r|Enter符|
|\\|反斜杠|
|\OOO|八进制整数，范围000~777|
|\xHHH|十六进制整数，范围00~FF|
|\uhhhh|十六进制编码的Unicode字符|

## Array(数组)
### 数组对象的作用
使用单独的变量名来存储一系列的值。

|数组方法|属性|
|-|-|
|var x=myCars.length|myCars 中元素的数量|
|var x=myCars.indexOf("Volvo")|"Volvo" 值的索引值|
```
1.arr.length:要获取Array的长度，直接访问length属性
2.arr.indexOf:Array可以通过indexOf()来搜索指定一个元素的位置
(undefined 超出范围的索引不会报错，但一律返回undefin)
3.slice:截取Array的部分元素，然后返回一个新的Array
```


### 字符串
```js
//可以通过indexOf()来搜索指定一个元素的位置
var str='中华人民共和国'
Console.log(str.indexOf("民")); //第几位，从0开始
```

```js
var str ='ak47'
Console.log(toUppercase); //输出内容为大写AK47
Console.log(str); //输出内容为ak47
```

### 数组
1.arr.length:要获取Array的长度，直接访问length属性
```js
let len =strNew.length;
Console.log(len); //表示数组长度
Console.log(strNew[10]); //第十位
```


```js
var str = '中华人民共和国'
Console.log(str.substring(2,4));
// 从0开始，人是第二位，民是第三位，第四位共不包含。所以输出内容为人民
Console.log(str.substring(str.length -2));
// 表示输出内容为 和国
```

```js
var arr=[1,9,18,[9,21]],{name:'亚硝酸盐'，age：18};
Console.log(arr[3]);
//表示套组里的第三位
输出内容是[9.21]
Console.log(arr[3][0]);
//表示第一个大套组里的第三位中第二个小套组里的第0位
输出结果位9
```

```js
//slice:截取Array的部分元素，然后返回一个新的Array
var arr =[1,9,18];
Console.log(arr.slice(0,1))
效果为1
```

